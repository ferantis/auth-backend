package sk.klu.auth.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sk.klu.auth.model.Contact;

@Repository
public interface ContactRepository extends CrudRepository<Contact, Long> {


}
