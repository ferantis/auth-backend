package sk.klu.auth.repository;

import org.springframework.data.repository.CrudRepository;
import sk.klu.auth.model.Customer;

import java.util.List;

public interface CustomerRepository extends CrudRepository<Customer, Long> {

    List<Customer> findByEmail(String email);
}
