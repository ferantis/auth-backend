package sk.klu.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

import javax.persistence.Entity;

@SpringBootApplication
@ComponentScans({
		@ComponentScan("sk.klu.auth.controller"),
		@ComponentScan("sk.klu.auth.config")
})
@EnableJpaRepositories("sk.klu.auth.repository")
@EntityScan("sk.klu.auth.model")
public class AuthApplication {

	public static void main(String[] args) {
		SpringApplication.run(AuthApplication.class, args);
	}

}
